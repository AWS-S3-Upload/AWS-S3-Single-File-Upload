﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using AWSSignatureV4_S3_Upload_Verification.Signers;

namespace AWSSignatureV4_S3_Upload_Verification
{
    /// <summary>
    /// Sample code showing how to POST objects to Amazon S3 with Signature V4 authorization https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-examples-using-sdks.html#sig-v4-examples-using-sdk-dotnet
    /// </summary>
    public static class PostS3ObjectAWSSignatureV4
    {
        static readonly string awsAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
        static readonly string awsSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
        static readonly string bucketName = ConfigurationManager.AppSettings["rootBucket"];
        static readonly string region = ConfigurationManager.AppSettings["region"];

        public static Uri GetUrl()
        {
            // Virtual hosted style addressing places the bucket name into the host address
            //var endpointUri = new Uri("https://"+bucketName+".s3-"+region+".amazonaws.com/" + objectKey);
            // Construct a virtual hosted style address with the bucket name part of the host address,
            // placing the region into the url if we're not using us-east-1.
            var regionUrlPart = string.Empty;
            if (!string.IsNullOrEmpty(region))
            {
                if (!region.Equals("us-east-1", StringComparison.OrdinalIgnoreCase))
                    regionUrlPart = string.Format("-{0}", region);
            }

            var endpointUri = new Uri(string.Format("https://{0}.s3{1}.amazonaws.com/{2}",
                                               bucketName,
                                               regionUrlPart,
                                               ""));
            return endpointUri;
        }

        public static List<PostS3AWSPolicyCondition> GetAWSCredentialV4(List<PostS3AWSPolicyCondition> conditions, string userName, string projectName)
        {
            // Use addedConditions to keep track of the new conditions we add herein
            List<PostS3AWSPolicyCondition> addedConditions = new List<PostS3AWSPolicyCondition>();

            var keyName = userName + @"/" + projectName + @"/";
            var dateTimeStamp = DateTime.UtcNow;

            //Console.WriteLine("PostS3ObjectSample:\r\nposting object 'SamplesPath/POSTedFile.txt' from bucket 'mysamplesbucket' ("+region+")");
            Console.WriteLine("PostS3ObjectSample:\r\nposting object to bucket '"+bucketName+"' (" + region + ")");

            var signer = new AWS4SignerForPOST
            {
                EndpointUri = GetUrl(),
                HttpMethod = "PUT",
                Service = "s3",
                Region = region
            };

            Dictionary<string, PostS3AWSPolicyCondition> basePolicyElements = new Dictionary<string, PostS3AWSPolicyCondition> {
            { "bucket", new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_EXACT, "bucket", bucketName, false) },
            { "key", new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_STARTS_WITH, "key", keyName) },
            { "acl", new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_EXACT, "acl", "public-read") },
            { "success_action_status", new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_EXACT, "success_action_status", "201") },
            { "Content-Type", new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_STARTS_WITH, "Content-Type", "") },
            { AWS4SignerBase.X_Amz_Meta_UUID, new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_EXACT, AWS4SignerBase.X_Amz_Meta_UUID, "14365123651274") },
            { AWS4SignerBase.X_Amz_Server_Side_Encryption, new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_EXACT, AWS4SignerBase.X_Amz_Server_Side_Encryption, "AES256") },
            { AWS4SignerBase.X_Amz_Credential, new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_EXACT, AWS4SignerBase.X_Amz_Credential, signer.FormatCredentialStringForPolicy(awsAccessKey, dateTimeStamp)) },
            { AWS4SignerBase.X_Amz_Algorithm, new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_EXACT, AWS4SignerBase.X_Amz_Algorithm, signer.FormatAlgorithmForPolicy) },
            { AWS4SignerBase.X_Amz_Date, new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_EXACT, AWS4SignerBase.X_Amz_Date, signer.FormatDateTimeForPolicy(dateTimeStamp)) },
            { AWS4SignerBase.X_Amz_Meta_Tag, new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_STARTS_WITH, AWS4SignerBase.X_Amz_Meta_Tag, "") }
        };

            // Add required policy elements https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-HTTPPOSTConstructPolicy.html 
            foreach (string key in basePolicyElements.Keys )
            {
                // If the current list of policy elements does not contain an element in the required list then add it here
                if( !conditions.Contains(basePolicyElements[key] ))
                {
                    conditions.Add(basePolicyElements[key]);
                    addedConditions.Add(basePolicyElements[key]);
                }
            }

            // Build the elements into an AWS Policy string
            PostS3AWSPolicyBuilder policyBuilder = new PostS3AWSPolicyBuilder(conditions, dateTimeStamp);
            string policy = policyBuilder.getPolicy();
            Console.WriteLine("\n\npolicy:\n" + policy);
            System.Diagnostics.Debug.WriteLine("\n\npolicy:\n" + policy);

            // hash the Base64 version of the policy document and pass this to the signer as the body hash
            //var policyStringBytes = Encoding.UTF8.GetBytes(policyBuilder.ToString());
            var policyStringBytes = Encoding.UTF8.GetBytes(policy);
            var base64PolicyString = System.Convert.ToBase64String(policyStringBytes);

            Console.WriteLine("\n\nbase64PolicyString:\n" + base64PolicyString);


            // Create headers to build signature and credentials
            var headers = new Dictionary<string, string>
            {
                // Leaving it empty for the POST
                //{AWS4SignerBase.X_Amz_Content_SHA256, contentHashString},
                //{"content-length", objectContent.Length.ToString()},
                //{"content-type", "text/plain"}
            };

            // Compute signature and credentials and add them to the list
            signer.ComputeSignature(headers,
                    "",   // no query parameters
                base64PolicyString,
                awsAccessKey,
                awsSecretKey,
                dateTimeStamp,
                addedConditions);

            return addedConditions;
        }
    }
}
