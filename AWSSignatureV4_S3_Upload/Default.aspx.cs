﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AWSSignatureV4_S3_Upload_Verification;

public partial class _Default : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        string userName = txtusername.Text;
        string projectName = txtprojectname.Text;
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        string userName = txtusername.Text;
        string projectName = txtprojectname.Text;

        buildFormAndPolicy();
    }

    protected void buildFormAndPolicy()
    {
        List<PostS3AWSPolicyCondition> conditions = new List<PostS3AWSPolicyCondition>();

        // Add existing and auto generated form input elements to the list
        foreach(string key in Request.Form.Keys)
        {
            PostS3AWSPolicyCondition condition = new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_STARTS_WITH, key, "");
            conditions.Add(condition);
        }

        List<PostS3AWSPolicyCondition> addedConditions = PostS3ObjectAWSSignatureV4.GetAWSCredentialV4(conditions, txtusername.Text, txtprojectname.Text);

        uploadpage.Action = PostS3ObjectAWSSignatureV4.GetUrl().ToString();

        // Loop through all the newly added conditions and add them to the Form
        foreach(PostS3AWSPolicyCondition condition in addedConditions)
        {
            if (condition.includeInHtmlForm)
            {
                HtmlInputText input = new HtmlInputText();
                input.ID = condition.name;
                if (input.ID.Equals("key"))
                    input.Value = condition.value + "${filename}";
                else
                    input.Value = condition.value;
                uploadpage.Controls.Add(input);
                uploadpage.Controls.Add(new LiteralControl("\n"));
            }
        }

        // Add file input form field last since AWS ignores all fields in the request after the file field
        HtmlInputFile fileInput = new HtmlInputFile();
        fileInput.ID = "file";
        fileInput.Name = "submit";
        //fileInput.Attributes.Add("multiple", "multiple");
        fileInput.Attributes.Add("OnClick", "btnUpload_Click");
        uploadpage.Controls.Add(fileInput);
        uploadpage.Controls.Add(new LiteralControl("\n"));
    }
}