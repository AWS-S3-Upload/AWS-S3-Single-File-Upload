﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
  <div style="top: 20%; left: 40%; width: 500px; position: absolute;">
            <h1>File Uploader</h1>
                  <form id="uploadpage" runat="server" method="POST" enctype="multipart/form-data">
            <table class="tblmain">
                <tr>

                    <td>Username
                    </td>
                    <td>
                        <asp:TextBox ID="txtusername" Text="user" runat="server"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td>Project Name
                    </td>
                    <td>
                        <asp:TextBox ID="txtprojectname" Text="user1" runat="server"></asp:TextBox>
                    </td>

                </tr>
            </table>
            <table class="tblmain2">
                <tr>
                    <td colspan="2">
                        <!-- The elements after this will be ignored -->
                        <div id="selectedFiles"></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnUpload" runat="server" Text="Submit" OnClick="btnUpload_Click" />
                    </td>
                    <label id="lblMessage"></label>
                </tr>
                <tr>
                    <td colspan="2">
                        <span id="Span1" runat="server" style="color: red"></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="progress">
                            <div class="bar" style="width: 0%;">Progress:</div>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <script src="jquery-3.2.1.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#homepage").append("test this form ready function");

        });
    </script>
</body>
</html>
