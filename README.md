﻿This application will load a file to S3 with AWS S4 Signature/Policy authentication.  The sigatureV4 code was derived from https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-examples-using-sdks.html#sig-v4-examples-using-sdk-dotnet.

To run:
1. Modify the App Setting keys in the Web.config file to your security credentials.  IMPORTANT: For a production application these should be removed from Web.config and you should use the credentils file to manage them instead (see https://aws.amazon.com/blogs/security/a-new-and-standardized-way-to-manage-credentials-in-the-aws-sdks/ )
2. Run Default.aspx
3. Enter Username and Project Name.  This will configure the File Uploader to place the file into the following S3 bucket directory (the directory structure under your S3 bucket must already exist):  <your_bucket_name>/<Username>/<Project Name>
4. Click "Submit" to prepare the upload configuration
5. Click "Choose File" to choose a file (you can only choose one at a time in this application)
6. Click "Submit" to upload the file
7. Verify that the file was uploaded to your S3 bucket